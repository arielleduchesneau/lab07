
document.addEventListener("DOMContentLoaded", function(event) {

//=================== Le jeu ===================

    class Jeu{

     constructor(idSvg,idPointage){
            this.s = Snap( idSvg);
            this.sortiePointage = document.querySelector( idPointage);
            this.grandeurCarre = 20;
            this.grandeurGrille = 15;
    }

    nouvellePartie(){
            this.finPartie();
            this.affichagePointage(1);
            this.pomme = new Pomme(this);
            this.serpent =  new Serpent(this);
    }

    finPartie(){
         if(this.pomme !== undefined){
             this.pomme.supprimePomme();
             this.pomme = undefined
         }
    }

    affichagePointage(lePointage){
            this.sortiePointage.innerHTML = lePointage
    }







    }
//=================== Le serpent ===================

    class Serpent{

        constructor(leJeu){
            this.leJeu = leJeu;
            this.currentX = -1;
            this.currentY = 0;
            this.nextMoveX = 1;
            this.nextMoveY = 0;
            this.serpentLongueur = 1;
            this.tbCarreSerent = [];
            this.touche = false
            this.vitesse = 250;
            this.timing = setInterval(this.controleSerpent.bind(this), this.vitesse);
            document.addEventListener('keydown',this.verifTouche.bind(this))
        }

        verifTouche(evt){
            var evt = evt;
            this.depacement(evt.keyCode);
        }

        depacement(dirCode){
            switch(dirCode){
                case 37:
                    this.nextMoveX = -1;
                    this.nextMoveY = 0;
                    break;
                case 38:
                    this.nextMoveX = 0;
                    this.nextMoveY = -1;
                    break;
                case 39:
                    this.nextMoveX = 1;
                    this.nextMoveY = 0;
                    break;
                case 40:
                    this.nextMoveX = 0;
                    this.nextMoveY = 1;
                    break;
            }
            //console.log(this.nextMoveX, this.nextMoveY)
        }

        controleSerpent(){
            var nextX = this.currentX + this.nextMoveX;
            var nextY = this.currentY + this.nextMoveY;
            this.tbCarreSerent.forEach(function (element) {
                if(nextX === element[1] && nextY === element[2]){
                    console.log("moi!");
                    this.leJeu.finPartie();
                    this.touche = true;
                }
            }.bind(this));
            if(nextY < 0|| nextX < 0 || nextY > this.leJeu.grandeurGrille-1 || nextX > this.leJeu.grandeurGrille-1 ){
                this.leJeu.finPartie();
                this.touche = true
            }
            if(!this.touche){
                this.dessineCarre(nextX,nextY);
                this.currentX = nextX;
                this.currentY = nextY;
            }
        }

        dessineCarre(x,y){
            var unCarre = [this.leJeu.s.rect(x * this.leJeu.grandeurCarre, y * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre)];
            this.tbCarreSerent.push(unCarre);

            if(this.tbCarreSerent.length > this.serpentLongueur){
                this.tbCarreSerent[0][0].remove();
                this.tbCarreSerent.shift();
            }
        }

        supprimeSerpent(){
        }




    }

//=================== La pomme ===================

    class Pomme{

        constructor(leJeu){
            this.leJeu = leJeu;
            this.pomme = [];
            this.ajoutePomme()
        }

        ajoutePomme(){
            var posX = Math.floor(Math.random() * this.leJeu.grandeurGrille);
            var posY = Math.floor(Math.random() * this.leJeu.grandeurGrille);
            this.pomme = [this.leJeu.s.rect(posX * this.leJeu.grandeurCarre, posY * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre).attr({fill:'pink'}), posX, posY];
        }

        supprimePomme(){
            this.pomme[0].remove();
        }







    }

//===================Code===================

    var unePartie = new Jeu("#jeu", "#pointage");
    var btnJouer = document.querySelector("#btnJouer");
    btnJouer.addEventListener('click', nouvellePartie);

    function nouvellePartie() {
        unePartie.nouvellePartie();
    }
});